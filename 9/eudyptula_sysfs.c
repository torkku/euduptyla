/*
 * Eudyptula Challenge task 09 sysfs module.
 *
 * Creates a new sysfs directory called "eudyptula"
 *
 * Under that directory creates a new virtual file called "id"
 * - when the file is read from returns "ad254f65b4d9"
 * - when the file is written to returns valid ret if the string written is
 *   "ad254f65b4d9", otherwise returns invalid value error
 * - file is readable by all and writable by root
 *
 * Under the same directory creates a new virtual file called "jiffies"
 * - when the file is read from returns the current value of kernel jiffies
 *   timer
 * - file is readable by all and writable by root
 *
 * Under the same directory creates a new virtual file called "foo"
 * - when the file is written to up to one page of the data is stored in kernel
 *   memory
 * - when read from the earlier written data (if any) is returned
 * - file is readable by all and writable by root
 *
 */
#include <linux/kobject.h>	/* kobject */
#include <linux/sysfs.h>	/* sys filesystem */
#include <linux/module.h>	/* all modules need this */
#include <linux/init.h>		/* for module init and exit macros */
#include <linux/jiffies.h>	/* for jiffies */
#include <linux/mutex.h>	/* mutexes for locking */

#define ID_LEN 13
static const char assigned_id[] = "ad254f65b4d9\n";

/* buffer for "foo" virtual file */
static char foo_buf[PAGE_SIZE] = {0};
/* mutex for controlling reads and write of "foo" virtual file */
static DEFINE_MUTEX(foo_mutex);

static ssize_t eudyptula_id_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	if (strncmp(buf, assigned_id, count))
		return -EINVAL;

	return count;
}

static ssize_t eudyptula_id_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return scnprintf(buf, PAGE_SIZE, "%s", assigned_id);
}

static ssize_t eudyptula_jiffies_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	/* copy jiffies value to destination buffer */
	return scnprintf(buf, PAGE_SIZE, "%ld\n", jiffies);
}

static ssize_t eudyptula_foo_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	ssize_t ret;

	if (count > sizeof(foo_buf) - 1)
		return -EINVAL;

	/* acquire lock to make sure no-one else is reading at the time */
	mutex_lock(&foo_mutex);

	/* clear the buffer */
	memset(foo_buf, 0x0, sizeof(foo_buf));

	/* read new value to foo_buf */
	ret = snprintf(foo_buf, sizeof(foo_buf), "%.*s",
		(int)min(count, sizeof(foo_buf) - 1), buf);

	if (ret < 0)
		goto unlock_and_ret;

	/* null terminate */
	foo_buf[ret] = '\0';

unlock_and_ret:
	mutex_unlock(&foo_mutex);

	return ret;
}

static ssize_t eudyptula_foo_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	ssize_t ret;

	/* acquire lock to make sure no-one else is writing at the time */
	mutex_lock(&foo_mutex);

	ret = scnprintf(buf, PAGE_SIZE, "%s", foo_buf);

	mutex_unlock(&foo_mutex);

	return ret;
}

static struct kobj_attribute id_attribute =
	__ATTR(id, 0664, eudyptula_id_show, eudyptula_id_store);
static struct kobj_attribute jiffies_attribute =
	__ATTR(jiffies, 0664, eudyptula_jiffies_show, NULL);
static struct kobj_attribute foo_attribute =
	__ATTR(foo, 0664, eudyptula_foo_show, eudyptula_foo_store);

static struct attribute *attrs[] = {
	&id_attribute.attr,
	&jiffies_attribute.attr,
	&foo_attribute.attr,
	NULL,
};

static struct attribute_group eudyptula_attr_group = {
	.attrs = attrs,
};

/* struct for holding our kobject */
static struct kobject *eudyptula_kobj;

static int __init eudyptula_sysfs_init(void)
{
	int retval;

	eudyptula_kobj = kobject_create_and_add("eudyptula", kernel_kobj);
	if (!eudyptula_kobj)
		return -ENOMEM;

	retval = sysfs_create_group(eudyptula_kobj, &eudyptula_attr_group);
	if (retval)
		kobject_put(eudyptula_kobj);

	return retval;
}

static void __exit eudyptula_sysfs_exit(void)
{
	kobject_put(eudyptula_kobj);
}

module_init(eudyptula_sysfs_init);
module_exit(eudyptula_sysfs_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ad254f65b4d9 <tikkuli5@gmail.com>");
MODULE_DESCRIPTION("eudyptula_sysfs -module for Eudyptula challenge task 09.");

