/*
 * Eudyptula Challenge task 08 debugfs.
 *
 * Creates a new debugfs directory called "eudyptula"
 *
 * Under that directory creates a new virtual file called "id"
 * - when the file is read from returns "ad254f65b4d9"
 * - when the file is written to returns valid ret if the string written is
 *   "ad254f65b4d9", otherwise returns invalid value error
 * - file is readable and writable by all
 *
 * Under the same directory creates a new virtual file called "jiffies"
 * - when the file is read from returns the current value of kernel jiffies
 *   timer
 * - file is readable by all
 *
 * Under the same directory creates a new virtual file called "foo"
 * - when the file is written to up to one page of the data is stored in kernel
 *   memory
 * - when read from the earlier written data (if any) is returned
 * - readable by all, writable by root
 *
 */
#include <linux/module.h>       /* all modules need this */
#include <linux/init.h>         /* for module init and exit macros */
#include <linux/uaccess.h>      /* interface with user space */
#include <linux/fs.h>           /* file operations */
#include <linux/debugfs.h>      /* debug filesystem */
#include <linux/jiffies.h>      /* for jiffies */
#include <linux/mutex.h>        /* mutexes for locking */

#define ID_LEN 13
static const char assigned_id[] = "ad254f65b4d9\n";

/* directory entry to hold our debugfs dir */
struct dentry *eudyptula_dir;

/* buffer for "foo" virtual file */
static char foo_buf[PAGE_SIZE] = {0};
/* mutex for controlling reads and write of "foo" virtual file */
static DEFINE_MUTEX(foo_mutex);

static ssize_t eudyptula_id_write(struct file *f, const char __user *buf,
				size_t count, loff_t *f_pos)
{
	char str[ID_LEN];
	ssize_t ret;

	ret = simple_write_to_buffer(str, ID_LEN, f_pos, buf, count);

	if (ret < 0)
		return ret;

	if (strncmp(str, assigned_id, ID_LEN))
		return -EINVAL;

	return ret;
}

static ssize_t eudyptula_id_read(struct file *f, char __user *buf, size_t count,
				loff_t *f_pos)
{
	return simple_read_from_buffer(buf, count, f_pos, &assigned_id, ID_LEN);
}

static ssize_t eudyptula_jiffies_read(struct file *f, char __user *buf,
				size_t count, loff_t *f_pos)
{
	char tmp[12];

	/* copy jiffies value to char buffer */
	sprintf(tmp, "%ld\n", jiffies);

	/* write buf to user */
	return simple_read_from_buffer(buf, count, f_pos, tmp, strlen(tmp));
}

static ssize_t eudyptula_foo_write(struct file *f, const char __user *buf,
				size_t count, loff_t *f_pos)
{
	ssize_t ret;

	if (count > sizeof(foo_buf) - 1)
		return -EINVAL;

	/* acquire lock to make sure no-one else is reading at the time */
	mutex_lock(&foo_mutex);

	/* clear the buffer */
	memset(foo_buf, 0x0, sizeof(foo_buf));

	/* read to buffer from user space */
	ret = simple_write_to_buffer(foo_buf, sizeof(foo_buf), f_pos, buf,
			count);

	if (ret < 0)
		goto unlock_and_ret;

	/* null terminate */
	foo_buf[ret] = '\0';

unlock_and_ret:
	mutex_unlock(&foo_mutex);

	return ret;
}

static ssize_t eudyptula_foo_read(struct file *f, char __user *buf,
				size_t count, loff_t *f_pos)
{
	ssize_t ret;

	/* acquire lock to make sure no-one else is writing at the time */
	mutex_lock(&foo_mutex);

	ret = simple_read_from_buffer(buf, count, f_pos, &foo_buf,
			strlen(foo_buf));

	mutex_unlock(&foo_mutex);

	return ret;
}


static const struct file_operations eudyptula_id_fops = {
	.read   = eudyptula_id_read,
	.write  = eudyptula_id_write,
};

static const struct file_operations eudyptula_jiffies_fops = {
	.read   = eudyptula_jiffies_read,
};

static const struct file_operations eudyptula_foo_fops = {
	.read   = eudyptula_foo_read,
	.write  = eudyptula_foo_write,
};

static int __init eudyptula_debugfs_init(void)
{
	/* create dir named "eudyptula" under /sys/kernel/debugfs */
	eudyptula_dir = debugfs_create_dir("eudyptula", NULL);

	if (!eudyptula_dir)
		return -ENOMEM;

	/* create "id" file. readable and writable by all */
	debugfs_create_file("id", S_IRUGO|S_IWUGO, eudyptula_dir, NULL,
			&eudyptula_id_fops);

	/* create "jiffies" file. readable by all */
	debugfs_create_file("jiffies", S_IRUGO, eudyptula_dir, NULL,
			&eudyptula_jiffies_fops);

	/* create "foo" file. readable by all, writable by root */
	debugfs_create_file("foo", S_IRUGO|S_IWUSR, eudyptula_dir, NULL,
			&eudyptula_foo_fops);

	return 0;
}

static void __exit eudyptula_debugfs_exit(void)
{
	/* cleanup */
	debugfs_remove_recursive(eudyptula_dir);
}

module_init(eudyptula_debugfs_init);
module_exit(eudyptula_debugfs_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ad254f65b4d9 <tikkuli5@gmail.com>");
MODULE_DESCRIPTION("eudyptula_debugfs -module for Eudyptula challenge task 08.");

