/*
 * Hello World! kernel module that prints to the kernel debug
 * log level "Hello World!" when loaded.
 */
#include <linux/module.h>       /* all modules need this */
#include <linux/kernel.h>       /* for KERN_DEBUG */
#include <linux/init.h>         /* for module init and exit macros */

static int __init hello_init(void)
{
    printk(KERN_DEBUG "Hello, world!\n");
    return 0;
}

static void __exit hello_exit(void)
{
    //printk(KERN_DEBUG "Hello, world module removed.\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ad254f65b4d9 <tikkuli5@gmail.com>");
MODULE_DESCRIPTION("Hello, World! -module for Eudyptula challenge #1.");

