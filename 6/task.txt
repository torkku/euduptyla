From little@www.eudyptula-challenge.org Mon Dec  1 23:08:50 2014
Delivered-To: tikkuli5@gmail.com
Received: by 10.27.174.20 with SMTP id x20csp332357wle;
        Mon, 1 Dec 2014 13:08:51 -0800 (PST)
X-Received: by 10.70.26.100 with SMTP id k4mr62399569pdg.101.1417468130379;
        Mon, 01 Dec 2014 13:08:50 -0800 (PST)
Return-Path: <little@www.eudyptula-challenge.org>
Received: from www.eudyptula-challenge.org (li684-192.members.linode.com. [23.239.3.192])
        by mx.google.com with ESMTP id b6si30737863pdk.59.2014.12.01.13.08.49
        for <tikkuli5@gmail.com>;
        Mon, 01 Dec 2014 13:08:50 -0800 (PST)
Received-SPF: none (google.com: little@www.eudyptula-challenge.org does not designate permitted sender hosts) client-ip=23.239.3.192;
Authentication-Results: mx.google.com;
       spf=none (google.com: little@www.eudyptula-challenge.org does not designate permitted sender hosts) smtp.mail=little@www.eudyptula-challenge.org
Received: by www.eudyptula-challenge.org (Postfix, from userid 1000)
	id A6693DB0D; Mon,  1 Dec 2014 21:09:10 +0000 (UTC)
Date: Mon, 1 Dec 2014 21:09:10 +0000
From: Little Penguin <little@eudyptula-challenge.org>
To: tikkuli5@gmail.com
Subject: [ad254f65b4d9] Task 06 of the Eudyptula Challenge
Message-ID: <20141201210910.GA20625@eudyptula-challenge.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Disposition: inline
User-Agent: Mutt/1.5.23 (2014-03-12)
Status: RO
Content-Length: 1650
Lines: 32

This is Task 06 of the Eudyptula Challenge
------------------------------------------

Nice job with the module loading macros.  Those are tricky, but a very
valuable skill to know about, especially when running across them in
real kernel code.

Speaking of real kernel code, let's write some!

The tasks this time are:
  - Take the kernel module you wrote for task 01, and modify it to be a
    misc char device driver.  The misc interface is a very simple way to
    be able to create a character device, without having to worry about
    all of the sysfs and character device registration mess.  And what a
    mess it is, so stick to the simple interfaces wherever possible.
  - The misc device should be created with a dynamic minor number, no
    need running off and trying to reserve a real minor number for your
    test module, that would be crazy.
  - The misc device should implement the read and write functions.
  - The misc device node should show up in /dev/eudyptula.
  - When the character device node is read from, your assigned id is
    returned to the caller.
  - When the character device node is written to, the data sent to the
    kernel needs to be checked.  If it matches your assigned id, then
    return a correct write return value.  If the value does not match
    your assigned id, return the "invalid value" error value.
  - The misc device should be registered when your module is loaded, and
    unregistered when it is unloaded.
  - Provide some "proof" this all works properly.

As you will be putting your id into the kernel module, of course you
haven't forgotten it, but just to be safe, it's "ad254f65b4d9".

