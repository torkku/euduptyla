/*
 * Eudyptula Challenge task 06 misc character device.
 *
 * The device appears as /dev/eudyptula
 * - when the device is read from returns "ad254f65b4d9"
 * - when the device is written to returns valid ret if the string written is
 *   "ad254f65b4d9", otherwise returns invalid value error
 *
 */
#include <linux/module.h>	/* all modules need this */
#include <linux/kernel.h>	/* for KERN_DEBUG */
#include <linux/init.h>		/* for module init and exit macros */
#include <linux/miscdevice.h>	/* for implementing misc char device */
#include <linux/uaccess.h>	/* interface with user space */
#include <linux/fs.h>		/* file operations */

static const char assigned_id[] = "ad254f65b4d9\n";
#define ID_LEN 13

static ssize_t eudyptula_write(struct file *f, const char __user *buf,
				size_t count, loff_t *f_pos)
{
	char str[ID_LEN];
	ssize_t ret;

	ret = simple_write_to_buffer(str, ID_LEN, f_pos, buf, count);

	if (ret < 0)
		return ret;

	if (strncmp(str, assigned_id, ID_LEN))
		return -EINVAL;

	return ret;
}

static ssize_t eudyptula_read(struct file *f, char __user *buf, size_t count,
				loff_t *f_pos)
{
	return simple_read_from_buffer(buf, count, f_pos, &assigned_id, ID_LEN);
}

static const struct file_operations eudyptula_fops = {
	.read	= eudyptula_read,
	.write	= eudyptula_write,
};

static struct miscdevice eudyptula_dev = {
	MISC_DYNAMIC_MINOR,
	"eudyptula",
	&eudyptula_fops,
};

static int __init eudyptula_init(void)
{
	return misc_register(&eudyptula_dev);
}

static void __exit eudyptula_exit(void)
{
	misc_deregister(&eudyptula_dev);
}

module_init(eudyptula_init);
module_exit(eudyptula_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ad254f65b4d9 <tikkuli5@gmail.com>");
MODULE_DESCRIPTION("/dev/eudyptula misc character device for Eudyptula challenge task #6.");

