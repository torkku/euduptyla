From little@www.eudyptula-challenge.org Thu Oct  9 03:11:42 2014
Delivered-To: tikkuli5@gmail.com
Received: by 10.66.151.164 with SMTP id ur4csp327074pab;
        Wed, 8 Oct 2014 17:11:43 -0700 (PDT)
X-Received: by 10.182.226.161 with SMTP id rt1mr16930916obc.18.1412813502748;
        Wed, 08 Oct 2014 17:11:42 -0700 (PDT)
Return-Path: <little@www.eudyptula-challenge.org>
Received: from www.eudyptula-challenge.org (li684-192.members.linode.com. [23.239.3.192])
        by mx.google.com with ESMTP id jt10si393606oeb.29.2014.10.08.17.11.42
        for <tikkuli5@gmail.com>;
        Wed, 08 Oct 2014 17:11:42 -0700 (PDT)
Received-SPF: none (google.com: little@www.eudyptula-challenge.org does not designate permitted sender hosts) client-ip=23.239.3.192;
Authentication-Results: mx.google.com;
       spf=neutral (google.com: little@www.eudyptula-challenge.org does not designate permitted sender hosts) smtp.mail=little@www.eudyptula-challenge.org
Received: by www.eudyptula-challenge.org (Postfix, from userid 1000)
	id DB8A79C402; Thu,  9 Oct 2014 00:13:04 +0000 (UTC)
Date: Thu, 9 Oct 2014 00:13:04 +0000
From: Little Penguin <little@eudyptula-challenge.org>
To: tikkuli5@gmail.com
Subject: [ad254f65b4d9] Task 04 of the Eudyptula Challenge
Message-ID: <20141009001304.GA9740@eudyptula-challenge.org>
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="YZ5djTAD1cGYuMQK"
Content-Disposition: inline
User-Agent: Mutt/1.5.23 (2014-03-12)
Status: RO
Content-Length: 3979
Lines: 129


--YZ5djTAD1cGYuMQK
Content-Type: text/plain; charset=us-ascii
Content-Disposition: inline


This is Task 04 of the Eudyptula Challenge
------------------------------------------

Wonderful job in making it this far.  I hope you have been having fun.
Oh, you're getting bored, just booting and installing kernels?  Well,
time for some pedantic things to make you feel that those kernel builds
are actually fun!

Part of the job of being a kernel developer is recognizing the proper
Linux kernel coding style.  The full description of this coding style
can be found in the kernel itself, in the Documentation/CodingStyle
file.  I'd recommend going and reading that right now.  It's pretty
simple stuff, and something that you are going to need to know and
understand.  There is also a tool in the kernel source tree in the
scripts/ directory called checkpatch.pl that can be used to test for
adhering to the coding style rules, as kernel programmers are lazy and
prefer to let scripts do their work for them...

Why a coding standard at all?  Because of your brain (yes, yours, not
mine, remember, I'm just some dumb shell scripts).  Once your brain
learns the patterns, the information contained really starts to sink in
better.  So it's important that everyone follow the same standard so
that the patterns become consistent.  In other words, you want to make
it really easy for other people to find the bugs in your code, and not
be confused and distracted by the fact that you happen to prefer 5
spaces instead of tabs for indentation.  Of course you would never
prefer such a thing, I'd never accuse you of that, it was just an
example, please forgive my impertinence!

Anyway, the tasks for this round all deal with the Linux kernel coding
style.  Attached to this message are two kernel modules that do not
follow the proper Linux kernel coding style rules.  Please fix both of
them up, and send them back to me as attachments in your response email.

What, you recognize one of these modules?  Imagine that, perhaps I was
right to accuse you of the using the "wrong" coding style :)

Yes, the logic in the second module is crazy, and probably wrong, but
don't focus on that, just look at the patterns here, and fix up the
coding style, do not remove lines of code.

As always, please remember to use your ID in the subject line when
responding to this task, so that I can figure out who to attribute it
to.  And if you forgot (which of course you have not, we've been through
all of this before), your id is "ad254f65b4d9".

--YZ5djTAD1cGYuMQK
Content-Type: text/x-c; charset=us-ascii
Content-Disposition: attachment; filename="hello.c"

/*
 * Hello World! kernel module that prints to the kernel debug
 * log level "Hello World!" when loaded.
 */
#include <linux/module.h>       /* all modules need this */
#include <linux/kernel.h>       /* for KERN_DEBUG */
#include <linux/init.h>         /* for module init and exit macros */

static int __init hello_init(void)
{
    printk(KERN_DEBUG "Hello, world!\n");
    return 0;
}

static void __exit hello_exit(void)
{
    //printk(KERN_DEBUG "Hello, world module removed.\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ad254f65b4d9 <tikkuli5@gmail.com>");
MODULE_DESCRIPTION("Hello, World! -module for Eudyptula challenge #1.");


--YZ5djTAD1cGYuMQK
Content-Type: text/x-c; charset=us-ascii
Content-Disposition: attachment; filename="coding_style.c"

#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/delay.h>
#include <linux/slab.h>

int do_work( int * my_int, int retval ) {
	int x;
	int y=*my_int;
	int z;
	
	for(x=0;x< * my_int;++x) {
		udelay(10);
	}

	if (y < 10 )
		// That was a long sleep, tell userspace about it
		printk("We slept a long time!");

	z = x * y;

	return z;
}

int
my_init (void)
{
	int x = 10;

	x = do_work(&x, x);

	return x;
}

void my_exit( void )
{
	return;
}

module_init(my_init);
module_exit(my_exit);

--YZ5djTAD1cGYuMQK--

