/*
 * Eudyptula challenge task 05.
 *
 * Hello world! USB keboard module that prints "Hello world!"
 * to the kernel debug log level when a USB keyboard is connected.
 * 
 */
#include <linux/module.h>       /* all modules need this */
#include <linux/kernel.h>       /* for KERN_DEBUG */
#include <linux/init.h>         /* for module init and exit macros */
#include <linux/usb.h>
#include <linux/hid.h>
#include <linux/usb/input.h>

#define DRIVER_AUTHOR "ad254f65b4d9 <tikkuli5@gmail.com>"
#define DRIVER_DESC "Hello, World! USB module for Eudyptula challenge #5."
#define DRIVER_LICENSE "GPL"

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE(DRIVER_LICENSE);

/* driver works with all usb keyboards */
static struct usb_device_id usb_kbd_hello_table [] = {
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,
		USB_INTERFACE_SUBCLASS_BOOT,
		USB_INTERFACE_PROTOCOL_KEYBOARD) },
	{ }	/* Terminating entry */
};

MODULE_DEVICE_TABLE(usb, usb_kbd_hello_table);

static int __init usb_kbd_hello_init(void)
{
	pr_info("Hello World!\n");
	return 0;
}

static void __exit usb_kbd_hello_exit(void)
{
}

module_init(usb_kbd_hello_init);
module_exit(usb_kbd_hello_exit);

