/*
 * Eudyptula challenge task 05.
 *
 * Hello world! USB keboard module that prints "Hello world!"
 * to the kernel debug log level when a USB keyboard is connected.
 * 
 */
#include <linux/module.h>       /* all modules need this */
#include <linux/kernel.h>       /* for KERN_DEBUG */
#include <linux/init.h>         /* for module init and exit macros */
#include <linux/usb.h>
#include <linux/hid.h>
#include <linux/usb/input.h>

#define DRIVER_AUTHOR "ad254f65b4d9 <tikkuli5@gmail.com>"
#define DRIVER_DESC "Hello, World! USB driver for Eudyptula challenge #5."
#define DRIVER_LICENSE "GPL"

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE(DRIVER_LICENSE);

static int usb_kbd_hello_probe(struct usb_interface *iface,
				const struct usb_device_id *id)
{
	/* do nothing but output Hello World! -message */
	pr_info("usb_kbd_hello_probe called\n");
	//pr_info("Hello, world!\n");
	return 0;
}

static void usb_kbd_hello_disconnect(struct usb_interface *interface)
{
	pr_info("usb_kbd_hello_disconnect called\n");
}

/* driver works with all usb keyboards */
static struct usb_device_id usb_kbd_hello_table[] = {
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,
		USB_INTERFACE_SUBCLASS_BOOT,
		USB_INTERFACE_PROTOCOL_KEYBOARD) },
	{ }	/* Terminating entry */
};

MODULE_DEVICE_TABLE(usb, usb_kbd_hello_table);

static struct usb_driver usb_kbd_hello_driver = {
	.name = "usbkbdhello",
	.id_table = usb_kbd_hello_table,
	.probe = usb_kbd_hello_probe,
	.disconnect = usb_kbd_hello_disconnect,
};

module_usb_driver(usb_kbd_hello_driver);

